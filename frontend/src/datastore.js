import clone from 'lodash/cloneDeep';
import lzstring from 'lz-string';
import {get, writable} from 'svelte/store';

import observed from './observable.js';
import migrate from './migrations.js';


export const DATA_KEY = "data";
export const SHARE_KEY = DATA_KEY;
// TODO: remove this and associated logic at some point
const _OLD_DATA_KEY = "values";

const DEFAULT = {
  __version__: 2,
  title: "",
  values: [],
};


export const getDefault = () => {
  return clone(DEFAULT);
}


// this needs to be a svelte writable if we want it reactive across components.
// TODO: get rid of the Proxy logic maybe?
export const data = writable(
  (function() {
    let _raw, _data;

    const url = new URL(window.location.href);
    _raw = url.searchParams.get(SHARE_KEY);

    if (_raw) {
      _data = parseDataBlob(_raw, true);
      // TODO: what if fail?
      // if (err instanceof SyntaxError) ...

      // TODO: don't overwrite existing local data without complaining?

      url.searchParams.delete(SHARE_KEY);
      history.replaceState(null, null, url.href);
    }
    else {
      _raw = window.localStorage.getItem(_OLD_DATA_KEY);
      if (_raw) {
        // old data key in use, fix.
        window.localStorage.setItem(DATA_KEY, _raw);
        window.localStorage.removeItem(_OLD_DATA_KEY);
      } else {
        _raw = window.localStorage.getItem(DATA_KEY);
      }

      if (_raw) _data = parseDataBlob(_raw);
    }

    return _data ? Object.assign(getDefault(), migrate(_data)) : getDefault();
  })()
);

data.subscribe( obj => {
  window.localStorage.setItem(DATA_KEY,
                              dumpDataBlob(obj));
});


export function dumpDataBlob(obj, web=false) {
  const func = (
    web ?
      lzstring.compressToEncodedURIComponent :
      lzstring.compressToUTF16
  );

  return func(JSON.stringify(obj));
}

export function parseDataBlob(str, web=false) {
  // TODO: [remove me] [temporary]
  // this function might receive non-compressed data currently
  if (str[0] == '[' || str[0] == '{')
    return JSON.parse(str);

  const func = (
    web ?
      lzstring.decompressFromEncodedURIComponent :
      lzstring.decompressFromUTF16
  );

  return JSON.parse(func(str));
}

export function getShareUrl() {
  const url = new URL(window.location.href);
  url.searchParams.set(SHARE_KEY, dumpDataBlob(get(data), true));
  return url;
}
