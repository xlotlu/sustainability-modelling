const migrations = {
  0: data => {
    return {
      title: "",
      data: data,
    };
  },

  1: data => {
    return {
      title: data['title'],
      values: data['data'],
    };
  },

  2: data => {
    return {
      title: data['title'],
      values: data['values'].map(v => {
        return {
          title: v,
          content: "",
        };
      }),
    };
  },
};


export default function(data) {
  let version = data.__version__;
  if (version === undefined) version = 0;

  while (true) {
    const migration = migrations[version];
    if (migration === undefined) break;

    data = migration(data);
    version += 1;
  }

  data.__version__ = version;
  return data;
}
