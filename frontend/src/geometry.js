const _PRECISION = 4,
      _d = 10 ** _PRECISION,
      _norm = num => Math.round(num * _d),
      normalise = num => _norm(num) / _d;



// fuzzy equality functions
export const fuzzy = {
  normalise: x => normalise(x),
  eq: (a, b) => _norm(a) == _norm(b),
  lt: (a, b) => _norm(a) < _norm(b),
  gt: (a, b) => _norm(a) > _norm(b),
};


export class Vertex {
  constructor(x, y) {
    this.x = x;
    this.y = y;
  }

  toString() {
    return `${normalise(this.x)},${normalise(this.y)}`;
  }
}


export class VertexCollection extends Array {
  #instances = {};

  add(x, y) {
    const v = new Vertex(x, y),
          k = v.toString();

    let instance = this.#instances[k];
    if (instance === undefined) {
      instance = v;
      const len = this.push(instance);
      instance.id = len - 1;
      this.#instances[k] = instance;
    }

    return instance;
  }
}


// export a default collection
export const vertices = new VertexCollection();
