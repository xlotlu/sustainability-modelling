import { vertices } from './geometry.js';


const pi = Math.PI,
      sin = Math.sin,
      cos = Math.cos,
      tan = Math.tan;


const MAX = 6,
      deg30 = pi / 6,
      deg60 = pi / 3;


const Corner = {
  A: 0,
  B: 1,
  C: 2,
  D: 3,
  E: 4,
  F: 5,
};


const Hexagon = function(len, collection=vertices) {
  /*
    By default the hexagon is laid out with one corner up.
    The first (0th) corner is at 2 o'clock.

    In rotated mode one edge is up, and the first corner is on the right.
  */
  let start = Corner.A,
      angle = -deg30;

  /* More tweakables. */
  let origin = null,
      rotated = false;


  /* Calculated at runtime, all calculations will be relative to the center. */
  let cx = 0,
      cy = 0;

  const R = len / cos(deg60) / 2;

  this.length = len;
  this.radius = R;
  // the bisector from the circle's center to a chord (edge)
  this.distance = R * sin(deg60);

  let _corners, _inner;

  function _cleanup() {
    _corners = [];
    _inner = null;
  }
  _cleanup();

  const calculate = function() {
    // calculates the starting angle, and the center if an origin exists

    angle = -deg30 + start * deg60;
    if (rotated) angle -= deg30;

    if (origin == null) return;

    const [x, y] = origin;

    const dx = sin(pi / 2 - angle) * R,
          dy = cos(pi / 2 - angle) * R;

    cx = x - dx;
    cy = y - dy;

    _cleanup();
  }

  this.center = function(x, y) {
    if (!arguments.length) return [cx, cy];

    origin = null;
    [cx, cy] = [x, y];

    _cleanup();
    return this;
  }

  this.rotate = function(yesno) {
    rotated = yesno !== undefined ? yesno : !rotated;
    calculate();

    return this;
  }

  this.startwith = function(corner) {
    if (!arguments.length) return start;

    start = corner;
    calculate();

    return this;
  }

  this.startfrom = function(x, y) {
    if (!arguments.length) return this.corners()[start];

    origin = toCoords(arguments);
    calculate();

    return this;
  }

  this.corners = function() {
    // TODO: this should always return the same order (Corner.A -> .F),
    // regardless of startwith

    if (_corners.length) return _corners;

    let a = angle;

    for (let i = 0; i < MAX; i++) {
      _corners.push(collection.add(
        cx + R * cos(a),
        cy + R * sin(a)
      ));
      a += deg60;
    }

    return _corners;
  }

  this.inner = function() {
    if (_inner != null) return _inner;

    let ang = pi / 6,
        d = len / sin(ang),
        _p = (d - len) / 2,
        inlen = 2 * _p * tan(ang);

    let inner = new Hexagon(inlen, collection).rotate(!rotated).center(cx, cy);

    _inner = inner;
    return inner;
  }

  this.innerconnections = function() {
    const outer = this.corners(),
          inner = this.inner().corners();

    const connections = [];

    for (let i = 0; i < MAX; i++) {
      // i is the inner index
      const rel = rotated ? [i, i + 1] : [i - 1, i];
      rel.forEach(function(j) {
        j = norm(j - start);
        connections.push([outer[j], inner[i]]);
      })
    }

    return connections;
  }
}

const toCoords = function(args) {
  // converts the given arguments to an array of [x, y]
  if (args.length == 1) {
    const arg = args[0];
    if (Array.isArray(arg))
      return arg;
    else
      return [arg.x, arg.y];
  }

  else if (args.length == 2)
    return args;

  else
    throw new Error("Invalid coordinates");
}

export const norm = function(x) {
  // normalizes access to node index
  if (x < 0) x += MAX
  if (x >= MAX) x -= MAX;
  return x;
}

const getPair = function(x, distance) {
  if (distance === undefined) distance = 1;
  return norm(x + distance);
}

const getPairs = function(distance) {
  if (distance === undefined) distance = 1;

  const pairs = [];
  for (let x = 0; x < MAX; x++) {
    pairs.push([x, getPair(x, distance)])
  }

  return pairs;
}

function connectionGenerator(distance) {
  return function() {
    const c = this.corners();
    return getPairs(distance).map(p => [c[p[0]], c[p[1]]])
  }
}

Hexagon.prototype.edges = connectionGenerator();
Hexagon.prototype.connections = connectionGenerator(2);
Hexagon.prototype._access = function() {
  // this makes sure all vertices are generated
  this.corners();
  this.inner().corners();
}


// the default fractolon implementation
class Fractolon extends Array {
  constructor(length, collection=vertices) {
    super();

    const main = new Hexagon(length, collection).center(0, 0);
    this.push(main);

    main.corners().forEach((c, i) => {
      const hexagon = new Hexagon(length / 3, collection).startfrom(c).startwith(i);
      this.push(hexagon);
    });

    const inner = new Hexagon(length / 3, collection).center(0, 0);
    this.push(inner);
  }
}
Fractolon.prototype._access = function() {
  for (const hexagon of this) hexagon._access();
}


export { Corner, Hexagon, Fractolon };
