import { writable } from 'svelte/store';


const DATA_KEY = "settings";

const _editor = ["inline", "full"],
      _buttons = ["left", "right"],
      _themes = ["white", "dark"];


const DEFAULT = {
  editor: 0,
  mouse: 0,
  theme: 0,
};


const _to_num = s => {
  const v = {false: 0, true: 1}[s];
  return v !== undefined ? v : s;
}

const _force_number = {
  get(target, property) {
    return target[_to_num(property)];
  },

  set(target, property, value) {
    return target[_to_num(property)] = value;
  },

  deleteProperty(target, property) {
    return delete target[_to_num(property)];
  },
};


export const editor = new Proxy(_editor, _force_number);
export const buttons = new Proxy(_buttons, _force_number);
export const themes = new Proxy(_themes, _force_number);



const settings = writable(
  (function() {
    const _raw = window.localStorage.getItem(DATA_KEY);
    let _data;
    try {
      _data = JSON.parse(_raw);
    }
    catch (e) {
      _data = {};
    }

    return Object.assign({}, DEFAULT, _data);
  })()
);


settings.subscribe( obj => {
  window.localStorage.setItem(DATA_KEY,
                              JSON.stringify(obj));

  const theme = themes[obj.theme];
  document.body.classList.toggle('dark', theme == 'dark');
})


export default settings;
