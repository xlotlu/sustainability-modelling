import sanitize from 'sanitize-filename';


export const downloadBlob = (blob, name) => {
  const link = document.createElement("a");
  link.href = URL.createObjectURL(blob);
  link.download = sanitize(name);

  document.body.appendChild(link);
  link.click()
  document.body.removeChild(link);
}


export const title = s => {
  return s[0].toUpperCase() + s.substr(1).toLowerCase();
}
