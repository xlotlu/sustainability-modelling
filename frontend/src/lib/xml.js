import { VertexCollection } from '../geometry.js';
import { Hexagon, Fractolon, norm } from '../hexagon.js';


const serializer = new XMLSerializer(),
      parser = new DOMParser();


export function dumpXML(data) {
  const doc = document.implementation.createDocument('', 'NodeContentCollection');

  const type = doc.createElement('STAR_TYPE');
  type.textContent = 'TYPE_1';
  doc.documentElement.appendChild(type);

  const nodes = doc.createElement('NodeContents');
  doc.documentElement.appendChild(nodes);

  const values = [];

  data.forEach( (item, i) => {
    const id = int2ext.get(i);
    values[id] = item;
  })

  values.forEach( (item, i) => {
    const node = doc.createElement('NodeContent');
    node.setAttribute('Number', i);
    node.setAttribute('Title', item || 'Empty Title');
    node.setAttribute('Content', 'Empty Content');
    nodes.appendChild(node);
  })

  return serializer.serializeToString(doc.documentElement);
}


export function parseXML(content) {
  const doc = parser.parseFromString(content, 'application/xml'),
        root = doc.documentElement;
  if (root.tagName != 'NodeContentCollection') {
    // TODO: unknown file format
    return;
  }

  const type = root.firstElementChild;
  if (type.tagName != 'STAR_TYPE' || type.textContent != 'TYPE_1') {
    // TODO: unknown file format
    return;
  }

  const out = [];
  for (const elem of root.querySelectorAll('NodeContents > NodeContent')) {
    let number = parseInt(elem.getAttribute('Number')),
        title = elem.getAttribute('Title'),
        content = elem.getAttribute('Content');

    if (title == "Empty Title") title = "";
    if (content == "Empty Content") content = "";

    const id = ext2int.get(number);
    out[id] = title;
  }

  return out;
}


// build the mapping between xml nodes and the internal implementation
const int2ext = new Map(),
      ext2int = new Map();

// can't build a mapping automatically because xml hexagons don't have
// a consistent origin, so would end up hardcoding anyway.

const _internal = [
  // outers
  [0, 12, 7, 13, 6, 14],
  [23, 1, 21, 8, 22, 7],
  [8, 32, 2, 30, 9, 31],
  [40, 9, 41, 3, 39, 10],
  [11, 49, 10, 50, 4, 48],
  [57, 6, 58, 11, 59, 5],
  [13, 22, 31, 40, 49, 58],
  // inners
  [15, 16, 17, 18, 19, 20],
  [24, 25, 26, 27, 28, 29],
  [33, 34, 35, 36, 37, 38],
  [42, 43, 44, 45, 46, 47],
  [51, 52, 53, 54, 55, 56],
  [60, 61, 62, 63, 64, 65],
  [66, 67, 68, 69, 70, 71],
];

const _external = [
  // outers
  [0, 1, 2, 3, 4, 5],
  [12, 13, 14, 15, 16, 2],
  [15, 23, 24, 25, 26, 27],
  [38, 26, 34, 35, 36, 37],
  [48, 49, 37, 45, 46, 47],
  [56, 4, 57, 48, 58, 59],
  [3, 16, 27, 38, 49, 57],
  // inners
  [7, 8, 9, 10, 11, 6],
  [18, 19, 20, 21, 22, 17],
  [29, 30, 31, 32, 33, 28],
  [44, 39, 40, 41, 42, 43],
  [55, 50, 51, 52, 53, 54],
  [60, 61, 62, 63, 64, 65],
  [66, 67, 68, 69, 70, 71],
];


_internal.forEach( (items, i) => {
  items.forEach( (item, j) => {
    if (!int2ext.has[item]) int2ext.set(item, _external[i][j]);
  })
})

for (const [k, v] of int2ext) {
  ext2int.set(v, k);
}
