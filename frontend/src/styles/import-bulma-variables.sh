#!/bin/sh

fname='bulma-variables-default.styl'

files='
    utilities/initial-variables.sass
    utilities/derived-variables.sass
    elements/button.sass
'

root='../../node_modules/bulma/sass'

d="$(dirname "$(realpath "$0")")"
r="$(realpath "$d/$root")"


process() {
    grep -iE '^\$[a-z0-9_-]+:' "$1" \
         | grep -v '\bmergeColorMaps\b' \
         | sed -r \
               -e 's/ *: */ = /' \
               -e 's/ *!default *$//' \
               -e 's/#\{([^\}]*)\}/\1/g' \
               -e 's/\bbulmaRgba\b/alpha/g'
}

> "$d/$fname"
for f in $files; do
    echo "// $f"
    process "$r/$f"
    echo
done >> "$d/$fname"
