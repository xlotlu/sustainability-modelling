export default function(obj, notify) {
  const handler = {
    get(target, property) {
      if (property === "__proxy") return true;
      return target[property];
    },

    set(target, property, value) {
      // avoid notifying when value hasn't changed
      if (target[property] === value) return true;

      if (value.__proxy === true || typeof value !== "object") {
        target[property] = value;
      } else {
        target[property] = observed(value);
      }

      // avoid double notify on array modification
      if (!(target instanceof Array && property === "length")) {
        notify('x');
      }

      return true;
    },

    deleteProperty(target, property) {
      delete target[property];
      notify();

      return true;
    },
  };

  function observed(o) {
    if (o === undefined || o === null) return o;

    // we need to deep-watch things, so we can't just proxy
    // the original object
    const _o = o instanceof Array ? [] : {};

    Object.keys(o).forEach(function(k) {
      const v = o[k];
      if (typeof v !== "object") {
        _o[k] = v;
      } else {
        _o[k] = observed(v);
      }
    });

    return new Proxy(_o, handler);
  }

  return observed(obj);
}
